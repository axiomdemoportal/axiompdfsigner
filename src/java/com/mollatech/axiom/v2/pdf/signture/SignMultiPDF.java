package com.mollatech.axiom.v2.pdf.signture;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Meta;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;
//import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.cert.Certificate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Manoj Sherkhane
 */
public class SignMultiPDF {

    static Logger log = Logger.getLogger(SignMultiPDF.class.getName());
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int TOP = 1;
    public static final int BOTTOM = 2;
    public String RESOURCE = "";
    public String RESOURCE2 = "";
    private static Properties g_sSettings = null;
    private static String g_filepath = null;

    
    static {
        Provider p = Security.getProvider("BC");
        if (p == null) {
            Security.addProvider(new BouncyCastleProvider());
        }

    }
//    static {
//        Security.addProvider(new BouncyCastleProvider());
//    }

//    public boolean signPdf(String userid,String sourceFile, String destFile, String certFile, String password, RemoteSigningInfo rsInfo, String imagepath)
//            throws IOException, com.itextpdf.text.DocumentException, GeneralSecurityException {
//
//        try{
//        byte[] fileKey = Base64.decode(certFile);
//
//        String fileKeyPassword = password;
//
//        // et ici sa "passPhrase"
//        //String fileKeyPassword  = "1088631" ;
//        // Creation d'un KeyStore
//        KeyStore ks = KeyStore.getInstance("pkcs12");
//        // Chargement du certificat p12 dans el magasin
//        ks.load(new ByteArrayInputStream(fileKey), fileKeyPassword.toCharArray());
//        String alias = (String) ks.aliases().nextElement();
//        // Recupération de la clef privée
//        PrivateKey key = (PrivateKey) ks.getKey(alias, fileKeyPassword.toCharArray());
//        // et de la chaine de certificats
//        Certificate[] chain = ks.getCertificateChain(alias);
//
//        // reader and stamper
//        com.itextpdf.text.pdf.PdfReader reader = new com.itextpdf.text.pdf.PdfReader(sourceFile);
//        FileOutputStream os = new FileOutputStream(destFile);
//        com.itextpdf.text.pdf.PdfStamper stamper = com.itextpdf.text.pdf.PdfStamper.createSignature(reader, os, '\0');
//        // appearance
//        com.itextpdf.text.pdf.PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
//
//        if (imagepath != null) {
//            com.itextpdf.text.Image I = com.itextpdf.text.Image.getInstance(imagepath);
//            appearance.setImage(I);
//        }
//
//        appearance.setReason(rsInfo.reason);
//        appearance.setLocation(rsInfo.clientLocation);
//          appearance.setLayer2Text("Name     : " + rsInfo.name+ "\n"+
//                                   "Designation : " + rsInfo.designation + "\n"+
//                                   "Reason    : " + rsInfo.reason + "\n"+
//                                   "Location  : " + rsInfo.clientLocation + "\n" +
//                                   "Date : "+ new Date().toString());
//        
//         PageAndLocation pLocation = this.getRectangle(reader);
//        
//        appearance.setVisibleSignature(pLocation.rect,pLocation.page,userid);
//        // digital signature
//        ExternalSignature es = new PrivateKeySignature(key, "SHA1", "BC");
//        ExternalDigest digest = new BouncyCastleDigest();
//        MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, MakeSignature.CryptoStandard.CMS);
//        
//        return true;
//        }catch(Exception ex){
//            ex.printStackTrace();
//        }
//        return false;
//    }
//    
    public boolean signPdf(String userid, String sourceFile, String destFile, String certFile, String password, RemoteSigningInfo rsInfo, String imagepath)
            throws IOException, DocumentException, Exception {
        String tsa_url = "";
        String timeStampEnabled = "";
        InputStream input = null;
        try {

            String sep = System.getProperty("file.separator");
            String usrhome = System.getProperty("catalina.home");

            if (usrhome == null) {
                usrhome = System.getenv("catalina.home");
            }

            if (usrhome == null) {
                usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            }

            usrhome += sep + "axiomv2-settings";
            if (g_sSettings == null) {
                g_sSettings = new Properties();
                g_filepath = usrhome + sep + "dbsetting.conf";
                input = new FileInputStream(g_filepath);
                g_sSettings.load(input);

            }

            if (g_sSettings != null) {
                timeStampEnabled = g_sSettings.getProperty("pki.timestamp.enabled");
                if (timeStampEnabled != null) {
                    if (!timeStampEnabled.equals("") && !timeStampEnabled.isEmpty()) {
                        tsa_url = g_sSettings.getProperty("pki.timestamp.url");
                    }
                    log.debug("tsa_url" + tsa_url);
                    log.debug("timestampenabled" + timeStampEnabled);
                } else {
                    timeStampEnabled = "no";
                }
            } else {
                timeStampEnabled = "no";
            }

            log.debug("Signing Data");
            byte[] fileKey = Base64.decode(certFile);

            String fileKeyPassword = password;

            KeyStore ks = KeyStore.getInstance("pkcs12");

            ks.load(new ByteArrayInputStream(fileKey), fileKeyPassword.toCharArray());
            String alias = (String) ks.aliases().nextElement();

            PrivateKey pk = (PrivateKey) ks.getKey(alias, fileKeyPassword.toCharArray());
            Certificate[] chain = ks.getCertificateChain(alias);

            PdfReader reader = new PdfReader(sourceFile);
            log.debug("Reading PDF");
            log.debug("Destination file");
            FileOutputStream os = new FileOutputStream(destFile);
            Map<String, String> moreInfo = new HashMap<String, String>();
            String PdfAuthor = rsInfo.qrCodeData;
            String PdfTile = rsInfo.clientLocation;
            moreInfo.put(Meta.PRODUCER, PdfTile);
            moreInfo.put(Meta.AUTHOR, PdfAuthor);
            PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0', null, true);
            log.debug("Stamping pdf");
            stamper.setMoreInfo(moreInfo);
            //  byte[] data = this.getQRCode(rsInfo.qrCodeData);
//            Image image = Image.getInstance(data);
//            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
//
//                PdfContentByte content = stamper.getUnderContent(i);
//                image.setAbsolutePosition(400f, 200f);
//
//                content.addImage(image);
////                content.addImage(image, i, i, i, i, i, i);
//            }

//            File imageFile= new File(imagepath);
//            String absolutePath = imageFile.getAbsolutePath();
//           String filePath = absolutePath.substring(0,absolutePath.lastIndexOf(File.separator));
//       
//            BufferedImage myImg1 = ImageIO.read(new File(imagepath));
//           BufferedImage  tempJPG = resizeImage(myImg1, myImg1.getWidth()-150, myImg1.getHeight()-150);
//            RESOURCE=filePath+File.separator+"imgSignResized.png";
//               File newFileJPG = new File(RESOURCE);
//                ImageIO.write(tempJPG, "jpg", newFileJPG);
//            BufferedImage myImg = ImageIO.read(new File(RESOURCE));
//       
//  BufferedImage newImage = new BufferedImage(myImg.getWidth(), myImg.getHeight()+160, myImg.getType());
//        
//            Graphics g = newImage.getGraphics();
//            g.setColor(Color.white);
//            g.fillRect(0, 0, myImg.getWidth(), myImg.getHeight()+190);
//            g.drawImage(myImg, 20, 0, null);
//            g.dispose();
//            RESOURCE2=filePath+File.separator+"imgSignSpaced.png";
//           ImageIO.write(newImage, "PNG", new File(RESOURCE2));
//            BufferedImage source = ImageIO.read(new File(RESOURCE2));
//            byte[] outputLogo = null;
//            
            // byte[] smallQR = ImageResizer.resize(data, outputLogo, 0.3);
            // BufferedImage logo = ImageIO.read(new ByteArrayInputStream(smallQR));
//            Graphics g = source.getGraphics();
//            g.drawImage(logo, 0, 0, null);
//            
//            ByteArrayOutputStream byteArr = new ByteArrayOutputStream();
//            ImageIO.write(source, "png", byteArr);
            // g.dispose();
            //byte[] finaloutput = byteArr.toByteArray();
            // appearance
//            PdfSignatureAppearance appearanceQR = stamper.getSignatureAppearance();
////            byte[] data = this.getQRCode(rsInfo.qrCodeData);
//            appearanceQR.setImage(Image.getInstance(data));
//            PageAndLocation pLocation = this.getRectangle(reader);
//            appearanceQR.setVisibleSignature(pLocation.rect, pLocation.page, userid);
            PdfSignatureAppearance appearance = stamper.getSignatureAppearance();

            //  appearance.setImage(Image.getInstance(finaloutput));
            appearance.setReason(rsInfo.reason);
//            appearance.setLocation(rsInfo.clientLocation);
            //changed
//              appearance.setReason(rsInfo.reason);
//             appearance.setLayer2Font(FontFactory.getFont(FontFactory.HELVETICA,12,BaseColor.BLUE));
//              appearance.setImage(Image.getInstance(RESOURCE2));
            // appearance.setLocation(rsInfo.clientLocation);
////            appearance.setLayer2Text("Name     : " + rsInfo.name + "\n"
//                    + "Designation : " + rsInfo.designation + "\n"
//                    + "Reason    : " + rsInfo.reason + "\n"
//                    + "Location  : " + rsInfo.clientLocation + "\n"
//                    + "Date : " + new Date().toString());
            // PageAndLocation pLocation = this.getRectangle(reader);
            //  appearance.setVisibleSignature(pLocation.rect, pLocation.page, userid);
            // digital signature

            //changeto add Time Stamp
            TSAClient tsc = null;
            if (timeStampEnabled.toLowerCase().equals("yes")) {
                tsc = new TSAClientBouncyCastle(tsa_url);
                log.debug("request to get timestamp");
                log.debug("time stamp value" + tsc);
            }
            ExternalSignature es = new PrivateKeySignature(pk, "SHA1", "BC");
            ExternalDigest digest = new BouncyCastleDigest();
            log.debug("Before digital Signature");
            if (tsc != null) {
                MakeSignature.signDetached(appearance, digest, es, chain, null, null, tsc, 0, CryptoStandard.CMS);
            } else {
                MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, CryptoStandard.CMS);
            }
            log.debug("After Digital signature");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Exception while signing a file " + ex.getMessage());
            throw new Exception(ex);
        }
    }

    public PageAndLocation getRectangle(PdfReader reader) {
        AcroFields fields = reader.getAcroFields();

        float left = 0.0f;
        float bTop = 0.0f;
        float bRight = 0.0f;
        float bBottom = 0.0f;
        float width = 0.0f;
        float height = 0.0f;

        float pageHeight = 0.0f;
        float top = 0.0f;
        float pageRight = 0.0f;
        float pageBottom = 0.0f;
        float pageWidth = 0.0f;
        int i = 0;
        int noOfpages = reader.getNumberOfPages();
        int page = noOfpages;
        for (String signame : fields.getSignatureNames()) {

            List<AcroFields.FieldPosition> positions = fields.getFieldPositions(signame);
            Rectangle rect = positions.get(0).position; // In points:
            left = rect.getLeft();
            bTop = rect.getTop();
            bRight = rect.getRight();
            bBottom = rect.getBottom();
            width = rect.getWidth();
            height = rect.getHeight();

            page = positions.get(0).page;
            Rectangle pageSize = reader.getPageSize(page);
            pageHeight = pageSize.getTop();
            pageRight = pageSize.getRight();
            pageBottom = pageSize.getBottom();
            pageWidth = pageSize.getWidth();
            i++;
        }

        PageAndLocation pLocation = new PageAndLocation();
        pLocation.page = page;

        if (i == 0) {

//             int page = reader.getAcroFields().
//            Rectangle pageSize = reader.getPageSize(page);
//            pageHeight = pageSize.getTop();
//            top = pageHeight - bTop;
//            pageRight = pageSize.getRight();
//            pageBottom = pageSize.getBottom();
//            pageWidth = pageSize.getWidth();
//            left = 32;
//            top = pageHeight - 62;
//            bBottom = top - 100;
//            bRight = left + 200;
//            left = 32;
//           // left=72;
//           // bRight=84;
//            bTop = 328;
//            bBottom = 128;
//            //bTop=600;
//            bBottom=350;
//            bRight = 232;
            left = 32;
//            bTop = 328;
//            bBottom = 128;
            bTop = 760;
            bBottom = 640;
            bRight = 257;

            Rectangle rect = new Rectangle(left, bBottom, bRight, bTop);
            System.out.println("left, bBottom, bRight, bTop" + left + "," + bBottom + "," + bRight + "," + bTop + ",");
            pLocation.rect = rect;
            return pLocation;
        }
        System.out.println("PageWidth" + pageWidth);
        if (bRight >= 440) {

            if (bBottom <= 50) {
                pLocation.page = page + 1;
                left = 32;
                bBottom = 660;
                bRight = 257;
                bTop = 710;
            } else {
                left = 32;
                bTop = bTop - 150;
                bBottom = bBottom - 160;
//            bBottom = 128;
                bRight = 257;
            }

//            left = bRight + 100;
//            bRight = left + 200;
            Rectangle rect = new Rectangle(left, bBottom, bRight, bTop);
            pLocation.rect = rect;
            System.out.println("left, bBottom, bRight, bTop" + left + "," + bBottom + "," + bRight + "," + bTop + ",");
            return pLocation;

        } //        if (bTop > pageHeight - 100) {
        //            bBottom = bBottom - 200;
        //            bTop = bTop - 200;
        //            Rectangle rect = new Rectangle(left, bBottom, bRight, bTop);
        //            pLocation.rect = rect;
        //            System.out.println("left, bBottom, bRight, bTop" + left + "," + bBottom + "," + bRight + "," + bTop + ",");
        //            return pLocation;
        //        } 
        else {

            left = bRight + 40;
            //  left = 252;
//            bTop = 328;
//            bBottom = 128;
            bRight = bRight * 2;

            Rectangle rect = new Rectangle(left, bBottom, bRight, bTop);
            pLocation.rect = rect;
            System.out.println("left, bBottom, bRight, bTop" + left + "," + bBottom + "," + bRight + "," + bTop + ",");
            return pLocation;
        }

    }

//    public PageAndLocation getRectangle(com.itextpdf.text.pdf.PdfReader reader) {
//        com.itextpdf.text.pdf.AcroFields fields = reader.getAcroFields();
//
//        float left = 0.0f;
//        float bTop = 0.0f;
//        float bRight = 0.0f;
//        float bBottom = 0.0f;
//        float width = 0.0f;
//        float height = 0.0f;
//
//        float pageHeight = 0.0f;
//        float top = 0.0f;
//        float pageRight = 0.0f;
//        float pageBottom = 0.0f;
//        float pageWidth = 0.0f;
//        int i = 0;
//        int page = 1;
//        for (String signame : fields.getSignatureNames()) {
//
//            java.util.List<com.itextpdf.text.pdf.AcroFields.FieldPosition> positions = fields.getFieldPositions(signame);
//            com.itextpdf.text.Rectangle rect = positions.get(0).position; // In points:
//            left = rect.getLeft();
//            bTop = rect.getTop();
//            bRight = rect.getRight();
//            bBottom = rect.getBottom();
//            width = rect.getWidth();
//            height = rect.getHeight();
//            int lastPageCount=reader.getNumberOfPages();
//            page = positions.get(lastPageCount-1).page;
//            com.itextpdf.text.Rectangle pageSize = reader.getPageSize(page);
//            pageHeight = pageSize.getTop();
////            top = pageHeight - bTop;
//            pageRight = pageSize.getRight();
//            pageBottom = pageSize.getBottom();
//            pageWidth = pageSize.getWidth();
//            i++;
//        }
//
//        PageAndLocation pLocation = new PageAndLocation();
//        pLocation.page = page;
//        if (i == 0) {
//
////             int page = reader.getAcroFields().
////            Rectangle pageSize = reader.getPageSize(page);
////            pageHeight = pageSize.getTop();
////            top = pageHeight - bTop;
////            pageRight = pageSize.getRight();
////            pageBottom = pageSize.getBottom();
////            pageWidth = pageSize.getWidth();
////            left = 32;
////            top = pageHeight - 62;
////            bBottom = top - 100;
////            bRight = left + 200;
//            left = 32;
//            bTop = 780;
//            bBottom = 680;
//            bRight = 232;
//            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
//            pLocation.rect = rect;
//            return pLocation;
//        }
//        if (bRight < pageWidth - 32) {
//            left = bRight + 100;
//            bRight = left + 200;
//            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
//            pLocation.rect = rect;
//            return pLocation;
//
//        }
//        if (bTop > pageHeight - 100) {
//            bBottom = bBottom + 200;
//            top = top + 200;
//            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, top);
//            pLocation.rect = rect;
//            return pLocation;
//        } else {
//            left = 32;
//            bTop = 780;
//            bBottom = 680;
//            bRight = 232;
//            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
//            pLocation.rect = rect;
//            return pLocation;
//        }
//
//    }
    public static BufferedImage resizeImage(final java.awt.Image image, int width, int height) {
//    int targetw = 0;
//    int targeth = 75;

//    if (width > height)targetw = 112;
//    else targetw = 50;
//
//    do {
//        if (width > targetw) {
//            width /= 2;
//            if (width < targetw) width = targetw;
//        }
//
//        if (height > targeth) {
//            height /= 2;
//            if (height < targeth) height = targeth;
//        }
//    } while (width != targetw || height != targeth);
        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();

        return bufferedImage;
    }

    public byte[] getQRCode(String data) {
        ByteArrayOutputStream out = QRCode.from(data)
                .to(ImageType.PNG).stream();

        try {
//            ByteArrayOutputStream fout = new ByteArrayOutputStream(out.toByteArray());

            byte[] image = out.toByteArray();

//            out.flush();
//            fout.close();
            return image;
        } catch (Exception e) {
            // Do Logging
            e.printStackTrace();
        }

        return null;
    }
    
    
     //added for page number wise signpdf
    public boolean signPdfBypageNo(String userid, int pageno, int cnt, String sourceFile, String destFile, String certFile, String password, RemoteSigningInfo rsInfo, String imagepath)
            throws IOException, com.itextpdf.text.DocumentException, GeneralSecurityException {

        try {
            byte[] fileKey = Base64.decode(certFile);

            String fileKeyPassword = password;

            // et ici sa "passPhrase"
            //String fileKeyPassword  = "1088631" ;
            // Creation d'un KeyStore
            KeyStore ks = KeyStore.getInstance("pkcs12");
            // Chargement du certificat p12 dans el magasin
            ks.load(new ByteArrayInputStream(fileKey), fileKeyPassword.toCharArray());
            String alias = (String) ks.aliases().nextElement();
            // Recupération de la clef privée
            PrivateKey key = (PrivateKey) ks.getKey(alias, fileKeyPassword.toCharArray());
            // et de la chaine de certificats
            Certificate[] chain = ks.getCertificateChain(alias);

            // reader and stamper
            com.itextpdf.text.pdf.PdfReader reader = new com.itextpdf.text.pdf.PdfReader(sourceFile);
            FileOutputStream os = new FileOutputStream(destFile);
            com.itextpdf.text.pdf.PdfStamper stamper = com.itextpdf.text.pdf.PdfStamper.createSignature(reader, os, '\0',null,true);
            // appearance
            com.itextpdf.text.pdf.PdfSignatureAppearance appearance = stamper.getSignatureAppearance();

            if (imagepath != null) {
                com.itextpdf.text.Image I = com.itextpdf.text.Image.getInstance(imagepath);
                appearance.setImage(I);
            }

            appearance.setReason(rsInfo.reason);
            appearance.setLocation(rsInfo.clientLocation);
            appearance.setLayer2Text("Name           : " + rsInfo.name           + "\n"
                    + "Designation  : " + rsInfo.designation    + "\n"
                    + "Reason         : " + rsInfo.reason         + "\n"
                    + "Location        : " + rsInfo.clientLocation + "\n"
                    + "Date              : " + new Date().toString());

             PageAndLocation pLocation = this.getRectangleNew(reader, pageno, cnt, rsInfo);

            appearance.setVisibleSignature(pLocation.rect, pLocation.page, userid);
            // digital signature
            ExternalSignature es = new PrivateKeySignature(key, "SHA1", "BC");
            ExternalDigest  digest = new BouncyCastleDigest();

            //added for timestamp
            String tsa_url = "";  //LoadSettings.g_sSettings.getProperty("tsa_url");
            //String tsa_url = "htttp://"; 
            try {
                if (tsa_url == null || tsa_url.isEmpty() == true) {
                    MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, MakeSignature.CryptoStandard.CMS);
                } else {
                    TSAClient tsc = null;
                    tsc = new TSAClientBouncyCastle(tsa_url);
                    MakeSignature.signDetached(appearance, digest, es, chain, null, null, tsc, 0, MakeSignature.CryptoStandard.CMS);
                }
                //end of addition

                return true;
            } catch (EOFException e) {

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

      public boolean signPdfByCordinates(String userid, int pageno, int cnt, String sourceFile, String destFile, String certFile, String password, RemoteSigningInfo rsInfo, String imagepath)
            throws IOException, com.itextpdf.text.DocumentException, GeneralSecurityException {
       String tsa_url = "";
        String timeStampEnabled = "";
        InputStream input = null;
        try {
            
              try {

            String sep = System.getProperty("file.separator");
            String usrhome = System.getProperty("catalina.home");

            if (usrhome == null) {
                usrhome = System.getenv("catalina.home");
            }

            if (usrhome == null) {
                usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            }

            usrhome += sep + "axiomv2-settings";
            if (g_sSettings == null) {
                g_sSettings = new Properties();
                g_filepath = usrhome + sep + "dbsetting.conf";
                input = new FileInputStream(g_filepath);
                g_sSettings.load(input);

            }

            if (g_sSettings != null) {
                timeStampEnabled = g_sSettings.getProperty("pki.timestamp.enabled");
                if (timeStampEnabled != null) {
                    if (!timeStampEnabled.equals("") && !timeStampEnabled.isEmpty()) {
                        tsa_url = g_sSettings.getProperty("pki.timestamp.url");
                    }
                    log.debug("tsa_url" + tsa_url);
                    log.debug("timestampenabled" + timeStampEnabled);
                } else {
                    timeStampEnabled = "no";
                }
            } else {
                timeStampEnabled = "no";
            }
              }catch(Exception ex)
              {
              ex.printStackTrace();
              }
            log.debug("Signing Data");
            
            byte[] fileKey = Base64.decode(certFile);
            String fileKeyPassword = password;
            KeyStore ks = KeyStore.getInstance("pkcs12");
            ks.load(new ByteArrayInputStream(fileKey), fileKeyPassword.toCharArray());
            String alias = (String) ks.aliases().nextElement();
            // Recupération de la clef privée
            PrivateKey key = (PrivateKey) ks.getKey(alias, fileKeyPassword.toCharArray());
            // et de la chaine de certificats
            Certificate[] chain = ks.getCertificateChain(alias);

            // reader and stamper
            com.itextpdf.text.pdf.PdfReader reader = new com.itextpdf.text.pdf.PdfReader(sourceFile);
            FileOutputStream os = new FileOutputStream(destFile);
            com.itextpdf.text.pdf.PdfStamper stamper = com.itextpdf.text.pdf.PdfStamper.createSignature(reader, os, '\0',null,true);
            // appearance
            com.itextpdf.text.pdf.PdfSignatureAppearance appearance = stamper.getSignatureAppearance();

            if (imagepath != null) {
                com.itextpdf.text.Image I = com.itextpdf.text.Image.getInstance(imagepath);
                appearance.setImage(I);
            }

            appearance.setReason(rsInfo.reason);
            appearance.setLocation(rsInfo.clientLocation);
            appearance.setLayer2Text("Name           : " + rsInfo.name           + "\n"
                    + "Designation  : " + rsInfo.designation    + "\n"
                    + "Reason         : " + rsInfo.reason         + "\n"
                    + "Location        : " + rsInfo.clientLocation + "\n"
                    + "Date              : " + new Date().toString());
              PageAndLocation pLocation = new PageAndLocation();
                 pLocation.page =rsInfo.pageno ;
                 Rectangle rect=new Rectangle(rsInfo.x1, rsInfo.y2, rsInfo.x2, rsInfo.y1);
                 pLocation.rect=rect;
             // PageAndLocation pLocation=this.getRectangleNew(reader, pageno, cnt, rsInfo);
            appearance.setVisibleSignature(pLocation.rect, pLocation.page, userid);
            ExternalSignature es = new PrivateKeySignature(key, "SHA1", "BC");
            ExternalDigest  digest = new BouncyCastleDigest();
            try {
                if (tsa_url == null || tsa_url.isEmpty() == true) {
                    MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, MakeSignature.CryptoStandard.CMS);
                    System.out.println("Signing Done Successfully");
                } else {
                    TSAClient tsc = null;
                    tsc = new TSAClientBouncyCastle(tsa_url);
                    MakeSignature.signDetached(appearance, digest, es, chain, null, null, tsc, 0, MakeSignature.CryptoStandard.CMS);
                     System.out.println("Signing Done Successfully With timeStamp");
                }
                //end of addition

                return true;
            } catch (EOFException e) {

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    
    //added for Specific Rectangle
    public PageAndLocation getRectangleNew(com.itextpdf.text.pdf.PdfReader reader, int pageno, int cnt, RemoteSigningInfo rsInfo) {
        com.itextpdf.text.pdf.AcroFields fields = reader.getAcroFields();

        float left = 0.0f;
        float bTop = 0.0f;
        float bRight = 0.0f;
        float bBottom = 0.0f;
        float width = 0.0f;
        float height = 0.0f;

        float pageHeight = 0.0f;
        float top = 0.0f;
        float pageRight = 0.0f;
        float pageBottom = 0.0f;
        float pageWidth = 0.0f;
        int i = 0;
        int page = pageno;
        for (String signame : fields.getSignatureNames()) {

            java.util.List<com.itextpdf.text.pdf.AcroFields.FieldPosition> positions = fields.getFieldPositions(signame);
            com.itextpdf.text.Rectangle rect = positions.get(0).position; // In points:
            left = rect.getLeft();
            bTop = rect.getTop();
            bRight = rect.getRight();
            bBottom = rect.getBottom();
            width = rect.getWidth();
            height = rect.getHeight();

            page = positions.get(0).page;
            com.itextpdf.text.Rectangle pageSize = reader.getPageSize(page);
            pageHeight = pageSize.getTop();
//            top = pageHeight - bTop;
            pageRight = pageSize.getRight();
            pageBottom = pageSize.getBottom();
            pageWidth = pageSize.getWidth();
            i++;
        }

        PageAndLocation pLocation = new PageAndLocation();
        pLocation.page = page;
        Rectangle crop = reader.getCropBox(page);
    
        if (i == 0) {
            if (rsInfo.alignLeftOrRight == LEFT && rsInfo.topOrbottom == TOP) {
                left = 32;
                bTop = 780;
                bBottom = 680;
                bRight = 232;
            } else if (rsInfo.alignLeftOrRight == LEFT && rsInfo.topOrbottom == BOTTOM) {
                left = 32;
                bTop = 132;
                bBottom = 32;
                bRight = 232;
            } else if (rsInfo.alignLeftOrRight == RIGHT && rsInfo.topOrbottom == BOTTOM) {
                left = 380;
                bTop = 132;
                bBottom = 32;
                bRight = 580;
            } else {
                left = 380;
                bTop = 780;
                bBottom = 680;
                bRight = 580;
            }

//             int page = reader.getAcroFields().
//            Rectangle pageSize = reader.getPageSize(page);
//            pageHeight = pageSize.getTop();
//            top = pageHeight - bTop;
//            pageRight = pageSize.getRight();
//            pageBottom = pageSize.getBottom();
//            pageWidth = pageSize.getWidth();
//            left = 32;
//            top = pageHeight - 62;
//            bBottom = top - 100;
//            bRight = left + 200;
//added for test
//PdfReader inputPdfReader = new PdfReader(new FileInputStream(input));
//
//            left = crop.getRight()+32;
//            bTop = 780;
//            bBottom = bTop - 100;
//            bRight = crop.getLeft()+132;
//for left bottom
//            left = 32;
//            bTop = 132;
//            bBottom = 32;
//            bRight = 232;
//for right bottom
//            left = 380;
//            bTop = 132;
//            bBottom = 32;
//            bRight = 580;
//for right
//            left = 380;
//            bTop = 780;
//            bBottom = 680;
//            bRight = 580;
//for left
//            left = 32;
//            bTop = 780;
//            bBottom = 680;
//            bRight = 232;
            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
            pLocation.rect = rect;
            return pLocation;
//        }
//        }else if(i == 1){
//             left = 32;
//            bTop = 780;
//            bBottom = 680;
//            bRight = 232;
//            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
//            pLocation.rect = rect;
//            return pLocation;
//        }
//        if (bRight < pageWidth - 32) {
//            left = bRight + 100;
//            bRight = left + 200;
//            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
//            pLocation.rect = rect;
//            return pLocation;
//
//        }
//        if (bTop > pageHeight - 100) {
//            bBottom = bBottom + 200;
//            top = top + 200;
//            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, top);
//            pLocation.rect = rect;
//            return pLocation;
//        } else {
//            left = 32;
//            bTop = 780;
//            bBottom = 680;
//            bRight = 232;
//            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
//            pLocation.rect = rect;
//            return pLocation;

        }
        
        
         if (rsInfo.alignLeftOrRight == LEFT && rsInfo.topOrbottom == TOP) {
//            if (bRight < pageWidth - 32) {
//                  left = bRight+10;
//                bTop = 780;
//                bBottom = 680;
//                bRight = 232;
//                 
//             }
          if (bRight < pageWidth - 32) {
            left = bRight + 100;
            bRight = left + 200;
            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
            pLocation.rect = rect;
            return pLocation;

        }
        if (bTop > pageHeight - 100) {
            bBottom = bBottom + 200;
            top = top + 200;
            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, top);
            pLocation.rect = rect;
            return pLocation;
        } else {
            left = 32;
            bTop = 780;
            bBottom = 680;
            bRight = 232;
            com.itextpdf.text.Rectangle rect = new com.itextpdf.text.Rectangle(left, bBottom, bRight, bTop);
            pLocation.rect = rect;
            return pLocation;
        }
               
            } else if (rsInfo.alignLeftOrRight == LEFT && rsInfo.topOrbottom == BOTTOM) {
                left = 32;
                bTop = 132;
                bBottom = 32;
                bRight = 232;
            } else if (rsInfo.alignLeftOrRight == RIGHT && rsInfo.topOrbottom == BOTTOM) {
                left = 380;
                bTop = 132;
                bBottom = 32;
                bRight = 580;
            } else {
                left = 380;
                bTop = 780;
                bBottom = 680;
                bRight = 580;
            }
        
        
        
        
//added for test

     
        if (bRight >= 440) {

            if (bBottom <= 50) {
                pLocation.page = page;
                 left = 380;
                bTop = 780;
                bBottom = 680;
                bRight = 580;
//                left = 32;
//                bBottom = 660;
//                bRight = 257;
//                bTop = 710;
            } else {
                
                  pLocation.page = page;
                left = 32;
                bTop = bTop - 150;
                bBottom = bBottom - 160;
//            bBottom = 128;
                bRight = 257;
            }

//            left = bRight + 100;
//            bRight = left + 200;
            Rectangle rect = new Rectangle(left, bBottom, bRight, bTop);
            pLocation.rect = rect;
            System.out.println("left, bBottom, bRight, bTop" + left + "," + bBottom + "," + bRight + "," + bTop + ",");
            return pLocation;

        } //        if (bTop > pageHeight - 100) {
        //            bBottom = bBottom - 200;
        //            bTop = bTop - 200;
        //            Rectangle rect = new Rectangle(left, bBottom, bRight, bTop);
        //            pLocation.rect = rect;
        //            System.out.println("left, bBottom, bRight, bTop" + left + "," + bBottom + "," + bRight + "," + bTop + ",");
        //            return pLocation;
        //        } 
        else {

//            left = bRight + 150;
//            //  left = 252;
////            bTop = 328;
////            bBottom = 128;
//            bRight = bRight * 2;
//          left = crop.getLeft()-100;
//            bTop = 780;
//            bBottom = bTop - 100;
//            bRight = crop.getLeft()+232;
//            left = 432;
//            bTop = 780;
//            bBottom = 680;
//            bRight = 632;
//            left = 32;
//            bTop = 132;
//            bBottom = 32;
//            bRight = 232;
//for right bottom
            left = 380;
            bTop = 132;
            bBottom = 32;
            bRight = 580;

            Rectangle rect = new Rectangle(left, bBottom, bRight, bTop);
            pLocation.rect = rect;
            System.out.println("left, bBottom, bRight, bTop" + left + "," + bBottom + "," + bRight + "," + bTop + ",");
            return pLocation;
        }

    }
    //end

}
