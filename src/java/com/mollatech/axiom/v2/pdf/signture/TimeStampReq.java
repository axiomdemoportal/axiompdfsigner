package com.mollatech.axiom.v2.pdf.signture;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;


public class TimeStampReq extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TimeStampReq.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/timestamp-reply");

        //String userid = "Pramod";
        String s = null;
        String reuestFileName = "";

        String tsp_response_file = request.getSession().getId();
        tsp_response_file += ".tsr";

        String tsp_request_file = request.getSession().getId();
        tsp_request_file += ".tsq";
         System.out.println("Time Stamp Reuqest file length"+tsp_request_file.length());
        String responseFileName = tsp_response_file;

        try {
            log.debug("Request MimeType" + request.getContentType());
            System.out.println("Request Params" + request.getParameterNames());
            System.out.println("Time Request"+ request.getContentLength());

            InputStream is = request.getInputStream();
            byte[] bytes = IOUtils.toByteArray(is);

            reuestFileName = tsp_request_file;//"/home/pramod/Music/Ashish/ts1.tsq";
            log.debug("File Name Taken");
            FileOutputStream fout = new FileOutputStream(new File(reuestFileName));
            fout.write(bytes);
            fout.close();
            System.out.println("File Written Successfully");
            System.out.println("Starting Executing commands");

           // Process p = Runtime.getRuntime().exec("openssl ts -reply -config /etc/pki/tls/openssl.cnf -queryfile " + reuestFileName + " -out " + responseFileName);
           Process p = Runtime.getRuntime().exec("openssl ts -reply -config /etc/ssl/openssl.cnf -queryfile " + reuestFileName + " -out " + responseFileName);
            System.out.println("Response Generated");
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            System.out.println("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }
            System.out.println("Here is the standard error of the command (if any):\n");

            int BUFSIZE = 4096;
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
            File file = new File(responseFileName);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(responseFileName);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/timestamp-reply";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(responseFileName)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();
            (new File(tsp_response_file)).delete();
            (new File(tsp_request_file)).delete();

        } catch (Exception ex) {
           ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
