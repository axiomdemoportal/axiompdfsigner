package com.mollatech.axiom.v2.pdf.signture;

public class RemoteSigningInfo {

  public static final int RAW_DATA = 1;
    public static final int PDF = 2;

    public int type;
    public String dataToSign;
    public String clientLocation;
    public String reason;
    public String name;
    public String designation;
    
    public String qrCodeData;
    public boolean bNotifyUser;
    public boolean bReturnSignedDocument;
    public boolean bAddTimestamp;
    public String referenceId;
    public int  alignLeftOrRight;
    public int topOrbottom;
   public int x1=0;
   public int x2=0;
   public int y1=0;
   public int y2=0;
   public int pageno=0;
    
    
}
