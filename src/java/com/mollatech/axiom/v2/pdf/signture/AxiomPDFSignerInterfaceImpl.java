  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.pdf.signture;



import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

/**
 *
 * @author mollatech2
 */
@WebService(endpointInterface = "com.mollatech.axiom.v2.pdf.signture.AxiomPDFSignerInterface")
public class AxiomPDFSignerInterfaceImpl implements AxiomPDFSignerInterface {

  
    @Resource
    WebServiceContext wsContext;

     public boolean SignAxiomPDF(String userid,int pageno,int cnt, String sourceFile, String destFile, String certFile, String password, RemoteSigningInfo rsInfo, String imagepath) throws AxiomException {
        try {
            SignMultiPDF sPdf = new SignMultiPDF();
            boolean result = sPdf.signPdfBypageNo(userid, pageno, cnt, sourceFile, destFile, certFile, password, rsInfo, imagepath);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new AxiomException(ex.getMessage());
        }
    }

    @Override
    public boolean SignAxiomPDFWithCordinates(String userid, int pageno, int cnt, String sourceFile, String destFile, String certFile, String password, RemoteSigningInfo rsInfo, String imagepath) throws AxiomException {
    try {
            SignMultiPDF sPdf = new SignMultiPDF();
            boolean result = sPdf.signPdfByCordinates(userid, pageno, cnt, sourceFile, destFile, certFile, password, rsInfo, imagepath);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new AxiomException(ex.getMessage());
        }
        
        
        
    }

    
    
}
