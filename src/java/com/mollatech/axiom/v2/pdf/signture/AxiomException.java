/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.pdf.signture;

import javax.xml.ws.WebFault;

@WebFault(faultBean = "com.mollatech.axiom.v2.core.utils.AxiomFaultBean")
public class AxiomException extends Exception {

    private static final long serialVersionUID = 122232232324L;

    private AxiomFaultBean faultBean;

    public AxiomException(String message) {
        super(message);
        this.faultBean = new AxiomFaultBean();
        this.faultBean.setFaultBean(message);
    }

    public AxiomException(String message, AxiomFaultBean faultBean, Throwable cause) {
        super(message, cause);
        this.faultBean = faultBean;
    }

    public AxiomException(String message, AxiomFaultBean faultBean) {
        super(message);
        this.faultBean = faultBean;
    }

    public AxiomFaultBean getFaultInfo() {
        return faultBean;
    }
}