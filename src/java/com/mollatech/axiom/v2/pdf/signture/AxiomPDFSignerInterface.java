package com.mollatech.axiom.v2.pdf.signture;



import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface AxiomPDFSignerInterface  {

  
    @WebMethod
    public boolean SignAxiomPDF(@WebParam(name = "userid")String userid,@WebParam(name = "pageno")int pageno,@WebParam(name = "cnt")int cnt,@WebParam(name = "sourceFile")String sourceFile, @WebParam(name = "destFile")String destFile, @WebParam(name = "certFile")String certFile, @WebParam(name = "password")String password,@WebParam(name = "rsInfo") RemoteSigningInfo rsInfo,@WebParam(name = "imagepath") String imagepath) throws AxiomException ;

    
     @WebMethod
    public boolean SignAxiomPDFWithCordinates(@WebParam(name = "userid")String userid,@WebParam(name = "pageno")int pageno,@WebParam(name = "cnt")int cnt,@WebParam(name = "sourceFile")String sourceFile, @WebParam(name = "destFile")String destFile, @WebParam(name = "certFile")String certFile, @WebParam(name = "password")String password,@WebParam(name = "rsInfo") RemoteSigningInfo rsInfo,@WebParam(name = "imagepath") String imagepath) throws AxiomException ;
    
}
